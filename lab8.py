# Brandon Mills Lab 8

from tkinter import *    
import time

class BouncingBall(Frame):    
    def __init__(self):    
        Frame.__init__(self)    
    
        self.master.title("Bouncing Ball CSC-131 Lab 7")    
    
        self.grid()    

        self.windowWidth = 800
        self.windowHeight = 400
    
        self.canvas = Canvas(self, bg = "white", width = self.windowWidth, height = self.windowHeight, bd=0, highlightthickness=0, relief="ridge")
        self.canvas.grid(row = 0, column = 0)    

        self.xPos0 = 0
        self.yPos0 = 0
        self.xPos1 = 20
        self.yPos1 = 20

        self.direction = "se"

        self.ballID = 1

        self.draw()

    def updateloop(self):
        while True:
            self.update()
            self.bounce()
            self.move()
            self.draw()
            time.sleep(0.015)

    def move(self):
        if self.direction == "se":
            self.xPos0 = self.xPos0 + 2
            self.yPos0 = self.yPos0 + 2
            self.xPos1 = self.xPos1 + 2
            self.yPos1 = self.yPos1 + 2
        elif self.direction == "sw":
            self.xPos0 = self.xPos0 - 2
            self.yPos0 = self.yPos0 + 2
            self.xPos1 = self.xPos1 - 2
            self.yPos1 = self.yPos1 + 2
        elif self.direction == "ne":
            self.xPos0 = self.xPos0 + 2
            self.yPos0 = self.yPos0 - 2
            self.xPos1 = self.xPos1 + 2
            self.yPos1 = self.yPos1 - 2
        elif self.direction == "nw":
            self.xPos0 = self.xPos0 - 2
            self.yPos0 = self.yPos0 - 2
            self.xPos1 = self.xPos1 - 2
            self.yPos1 = self.yPos1 - 2
    
    def bounce(self):
        if self.direction == "se":
            if self.xPos1 >= self.windowWidth:
                self.squish("right")
                self.direction = "sw"
            if self.yPos1 >= self.windowHeight:
                self.squish("down")
                self.direction = "ne"
        if self.direction == "sw":
            if self.xPos0 <= 0:
                self.squish("left")
                self.direction = "se"
            if self.yPos1 >= self.windowHeight:
                self.squish("down")
                self.direction = "nw"
        if self.direction == "ne":
            if self.xPos1 >= self.windowWidth:
                self.squish("right")
                self.direction = "nw"
            if self.yPos0 <= 0:
                self.squish("up")
                self.direction = "se"
        if self.direction == "nw":
            if self.xPos0 <= 0:
                self.squish("left")
                self.direction = "ne"
            if self.yPos0 <= 0:
                self.squish("up")
                self.direction = "sw"

    def draw(self):
        self.canvas.delete(self.ballID)
        self.canvas.create_oval(self.xPos0, self.yPos0, self.xPos1, self.yPos1, fill="blue", tags="ball")
        self.ballID = self.canvas.find_withtag("ball")[0]

    def squish(self, direction):
        for i in range (3):
            self.update()
            if direction == "right":
                self.xPos0 = self.xPos0 + 4
                self.yPos0 = self.yPos0 - 2
                self.yPos1 = self.yPos1 + 2
            elif direction == "left":
                self.xPos1 = self.xPos1 - 4
                self.yPos0 = self.yPos0 - 2
                self.yPos1 = self.yPos1 + 2
            elif direction == "up":
                self.yPos1 = self.yPos1 - 4
                self.xPos0 = self.xPos0 - 2
                self.xPos1 = self.xPos1 + 2
            elif direction == "down":
                self.yPos0 = self.yPos0 + 4
                self.xPos0 = self.xPos0 - 2
                self.xPos1 = self.xPos1 + 2
            self.draw()
            time.sleep(0.035)
        for i in range(3):
            self.update()
            if direction == "right":
                self.xPos0 = self.xPos0 - 4
                self.yPos0 = self.yPos0 + 2
                self.yPos1 = self.yPos1 - 2
            elif direction == "left":
                self.xPos1 = self.xPos1 + 4
                self.yPos0 = self.yPos0 + 2
                self.yPos1 = self.yPos1 - 2
            elif direction == "up":
                self.yPos1 = self.yPos1 + 4
                self.xPos0 = self.xPos0 + 2
                self.xPos1 = self.xPos1 - 2
            elif direction == "down":
                self.yPos0 = self.yPos0 - 4
                self.xPos0 = self.xPos0 + 2
                self.xPos1 = self.xPos1 - 2
            self.draw()
            time.sleep(0.035)

def main():
    bb = BouncingBall()
    bb.updateloop()

main()  
